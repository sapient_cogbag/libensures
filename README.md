# libensures

Libensures (and the associated program) provides the ability to ensure that a file has certain data in it without wrecking the whole file. It also has templating capability.